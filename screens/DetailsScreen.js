import { Button, StyleSheet, Text, View } from'react-native';

const DetailsScreen = ({route, navigation}) => {

    const {id, name, age} = route.params;
    
    return (
        <View style={styles.container}>
            <Text>This is your details</Text>
            <Text>ID : {id}</Text>
            <Text>Name : {name}</Text>
            <Text>Age : {age}</Text>
            <Button
                title="Return to Home"
                onPress={() => navigation.navigate('Home')}
            />
        </View>
    );
};

export default DetailsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});