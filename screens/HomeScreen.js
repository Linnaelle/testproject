import React, { useState, useEffect, useRef } from'react';
import { Button, StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';

// Affichage de la liste des cocktails avec une pagination / FlatList
const HomeScreen = ({navigation}) => {
  const [cocktail, setCocktail] = useState([]);
  const [listCocktails, setListCocktails] = useState([]);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isFirstPageReceived, setIsFirstPageReceived] = useState(false);
  const nextPageIdentifierRef = useRef();
  
  // Récupérer l'ensemble des cocktails
  const getAllCocktails = async () => {
    setIsLoading(true);
    const response = await fetch('www.thecocktaildb.com/api/json/v1/1/search.php?f=a');
    const data = await response.json();
    setListCocktails(data.drinks);
  };

  
  const ListEndLoader = () => {
    if (!isFirstPageReceived && isLoading) {
      return <ActivityIndicator size={'large'} />;
    }
  };

  // Afficher la liste des cocktails
  const renderItem = ({ item }) => {
    return (
      <View>
        <Text>{item}</Text>
      </View>
    );
  };

  // Récupérer la prochaine page de cocktails
  useEffect(() => {
    getAllCocktails();
  }, []);
  
  return (
    <View style={styles.container}>
      <Text>Home Screen</Text>
      <Button
        title="Your profile"
        onPress={() => navigation.navigate('Profile', {
          id: 1,
          name: "Clément Perret",
          age: 25,
        })}
      />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});