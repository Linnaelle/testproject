import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

// Tab navigation import
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// Components import
import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import SettingsScreen from './screens/SettingsScreen';

// Tab icons import
import Entypo from '@expo/vector-icons/Entypo';
import Ionicons from '@expo/vector-icons/Ionicons';
import { AntDesign } from '@expo/vector-icons';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

/* [ INSTALL NAVIGATION ]
 * ----------------------------------------------------------------
 * npm i @react-navigation/native
 * npx expo i react-native-screens react-native-safe-area-context
 * npm i @react-navigation/native-stack
 * npx expo -c : si bug, reset le cache d'expo
 * npm i @react-navigation/bottom-tab : tab navigation
 */

export default function App() {
  return (
    <NavigationContainer>
      {/* <Stack.Navigator
        // Les options peuvent être ajoutées uniquement sur 1 seul screen
        screenOptions={{
          headerStyle: {
            backgroundColor: 'orange',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      >
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: "Welcome", // Changer le titre du Screen
          }}
        />
        <Stack.Screen
          name="Profile"
          component={DetailsScreen}
          options={({route}) => ({
            title: route.params.name, // Changer le titre du Screen via le paramètre << name >>
          })}
        />
      </Stack.Navigator> */}
      <Tab.Navigator
        screenOptions={{
          tabBarActiveTintColor: 'tomato',
          tabBarStyle: {
            backgroundColor: 'green',
          },
        }}
      >
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarIcon: ({color, size}) => ( // https://icons.expo.fyi/Index
              <Entypo name="home" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Profile"
          component={DetailsScreen}
          options={({route}) => ({
            title: route.params?.name || "Profile", // Changer le titre du Screen via le paramètre << name >> ou mettre "Profile" par défaut
            tabBarIcon: ({color, size}) => ( // https://icons.expo.fyi/Index
              <AntDesign name="idcard" size={size} color={color} />
            ),
          })}
        />
        <Tab.Screen
          name="Settings"
          component={SettingsScreen}
          options={{
            tabBarIcon: ({color, size}) => ( // https://icons.expo.fyi/Index
              <Ionicons name="settings" size={size} color={color} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
